import logging
import time
import ssl
import paho.mqtt.client as mqtt

from config import *
from decoders import decoders


def get_timestamp():
    return time.time()


def on_connect(client, userdata, flags, rc):
    if rc != 0:
        logging.fatal("Failed to connect to MQTT broker")
        exit(-1)
    logging.info("Successfully connected to MQTT")
    logging.info(decoders)
    for topic, callback in decoders.items():
        logging.info("Subscribing to %s -> %s" % (topic, callback))
        client.subscribe(topic)
        client.message_callback_add(topic, callback)


def on_message(client, userdata, msg):
    logging.warning("unexpected message : %s %s" % (msg.topic, str(msg.payload)))


def main():
    logging.basicConfig(level=logging.INFO)
    logging.info("Starting MQTT bridge - %s" % get_timestamp())

    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message

    if HAS_TLS_CERTS:
        client.tls_set(
            ca_certs=CERTS_PATH + CA_FILE,
            certfile=CERTS_PATH + CRT_FILE,
            keyfile=CERTS_PATH + KEY_FILE,
            cert_reqs=ssl.CERT_REQUIRED,
            tls_version=ssl.PROTOCOL_TLS,
            ciphers=None)

    client.connect(MQTT_SERVER, MQTT_PORT, 60)
    client.loop_forever()


if __name__ == '__main__':
    main()
