HAS_TLS_CERTS = True
CERTS_PATH = '/path/to/cert/files'
CA_FILE = "ca.crt"
CRT_FILE = "myfile.crt"
KEY_FILE = "myfile.key"
MQTT_SERVER = "my.mqtt.server.net"
MQTT_PORT = 8883
