import base64
import json
import camdetection_pb2
from google.protobuf.timestamp_pb2 import Timestamp


def format_if_not_zero(key,value):
    if (value is not None) and (value != 0) :
        return " %s:%d"%(key,value)
    else:
        return ""


def decoder_smartcam_device(client, userdata, msg):
    payload = json.loads(msg.payload)
    detection = camdetection_pb2.camdetection()
    detection.ParseFromString(base64.b64decode(payload["data"]))
    ts = Timestamp()
    ts.FromSeconds(detection.timestamp)

    print("%s:%s%s%s"%(ts.ToDatetime(),
                       format_if_not_zero("car", detection.nb_car),
                       format_if_not_zero("person", detection.nb_person),
                       format_if_not_zero("truck", detection.nb_truck),
                       )
          )

    decoded_payload = {
        'shot_date' : detection.timestamp,
        'nb_car'    : detection.nb_car,
        'nb_person' : detection.nb_person,
        'nb_truck'  : detection.nb_truck
    }
    client.publish(msg.topic+"/decoded", json.dumps(decoded_payload))

decoders = {
    "application/30/device/+/rx": decoder_smartcam_device
}
